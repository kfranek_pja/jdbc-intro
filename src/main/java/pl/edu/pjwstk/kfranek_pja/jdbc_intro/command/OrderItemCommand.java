package pl.edu.pjwstk.kfranek_pja.jdbc_intro.command;

import java.sql.SQLException;

import pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain.OrderItem;

public class OrderItemCommand extends IdentityCommand {

	public OrderItemCommand() {
		super("order_item", "CREATE TABLE order_item("
				         + "id bigint GENERATED BY DEFAULT AS IDENTITY, "
				         + "name varchar(255), "
				         + "description varchar(255), "
				         + "price double"
				         + ")",
				"INSERT INTO order_item VALUES (NULL, ?, ?, ?)",
				"UPDATE order_item SET "
				+ "name = ?, "
				+ "description = ?, "
				+ "price = ? "
				+ "WHERE id = ?");
	}

	public int insert(OrderItem orderItem){		
		int count = 0;
		try {
			this.insertStmt.setString(1, orderItem.getName());
			this.insertStmt.setString(2, orderItem.getDescription());
			this.insertStmt.setDouble(3, orderItem.getPrice());
			count = this.insertStmt.executeUpdate();
			orderItem.setId(this.getLastInsertId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}
	
	public int update(OrderItem orderItem){	
		if(orderItem.getId() == -1){
			return this.insert(orderItem);
		}
		
		
		int count = 0;
		try {
			this.updateStmt.setString(1, orderItem.getName());
			this.updateStmt.setString(2, orderItem.getDescription());
			this.updateStmt.setDouble(3, orderItem.getPrice());
			this.updateStmt.setLong(4, orderItem.getId());
			count = this.updateStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}

}
