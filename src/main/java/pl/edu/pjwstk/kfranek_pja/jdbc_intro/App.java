package pl.edu.pjwstk.kfranek_pja.jdbc_intro;

import java.util.ArrayList;

import pl.edu.pjwstk.kfranek_pja.jdbc_intro.command.AddressCommand;
import pl.edu.pjwstk.kfranek_pja.jdbc_intro.command.ClientDetailsCommand;
import pl.edu.pjwstk.kfranek_pja.jdbc_intro.command.OrderCommand;
import pl.edu.pjwstk.kfranek_pja.jdbc_intro.command.OrderItemCommand;
import pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain.Address;
import pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain.ClientDetails;
import pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain.Order;
import pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain.OrderItem;

/**
 * Hello world!
 *
 */

public class App 
{
    public static void main( String[] args )
    {
    	//AddressQuery aq = new AddressQuery();
    	
    	AddressCommand ac = new AddressCommand();
    	ClientDetailsCommand cdc = new ClientDetailsCommand();
    	OrderItemCommand oic = new OrderItemCommand();
    	OrderCommand oc = new OrderCommand(oic, ac, cdc);
    	
    	//Przykladowy adres
    	Address a = new Address();
    	a.setStreet("Dylinki");
    	a.setBuildingNumber("1");
    	a.setFlatNumber("2");
    	a.setPostalCode("80-880");
    	a.setCity("Gdańsk");
    	a.setCountry("PL");
    	
    	//Przykladowy klient
    	ClientDetails c = new ClientDetails();
    	c.setLogin("user");
    	c.setName("Jan");
    	c.setSurname("Testowy");
    	
    	//Przykladowe produkty
    	ArrayList<OrderItem> oi = new ArrayList<OrderItem>();
    	
    	OrderItem oi1 = new OrderItem();
    	oi1.setName("Zapalki");
    	oi1.setDescription("Sztromowki");
    	oi1.setPrice(0.1);
    	oi.add(oi1);
    	
    	OrderItem oi2 = new OrderItem();
    	oi2.setName("Gumka recepturka");
    	oi2.setDescription("Najlepsza");
    	oi2.setPrice(1.22);
    	oi.add(oi2);
    	
    	OrderItem oi3 = new OrderItem();
    	oi3.setName("Ryza papieru");
    	oi3.setDescription("A4");
    	oi3.setPrice(22);
    	oi.add(oi3);
    	
    	//Zamowienie
    	Order o = new Order();
    	o.setClient(c);
    	o.setDeliveryAddress(a);
    	o.setItems(oi);
    	
    	//Zapis do bazy
    	oc.insert(o);
    	
    	//Zmien pare rzeczy
    	oi2.setDescription("A5");
    	c.setLogin("userrr");
    	
    	OrderItem oi4 = new OrderItem();
    	oi4.setName("Worek ryżu");
    	oi4.setDescription("5 kg");
    	oi4.setPrice(10.5);
    	oi.add(oi4);
    	
    	a.setBuildingNumber("123");
    	
    	//Zaktualizuj
    	oc.update(o);
    	
    	
    	System.out.println(o.getId());
    	
    }
}
