package pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain;

//Szczegoly danego klienta

public class ClientDetails implements Identifiable  {
	
	private long id = -1;
	private String login;
	private String name;
	private String surname;
	
	public ClientDetails() {
	}
	
	public long getId(){
		return this.id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return this.surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getLogin() {
		return this.login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
}