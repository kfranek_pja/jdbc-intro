package pl.edu.pjwstk.kfranek_pja.jdbc_intro.command;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain.Identifiable;

public class IdentityCommand {
	protected Connection connection;
	protected Statement statement;

	private String url = "jdbc:hsqldb:hsql://localhost/testdb";
	protected PreparedStatement deleteStmt;
	protected PreparedStatement truncateStmt;
	protected PreparedStatement insertStmt;
	protected PreparedStatement updateStmt;
	protected String tableName;
	
	
	public IdentityCommand(String tableName, String createQuery, String insertQuery, String updateQuery) {
		try {
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();
			this.tableName = tableName;

			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tableExists = false;
			while (rs.next()) {
				if (tableName.equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}

			if (!tableExists)
				statement.executeUpdate(createQuery);

			this.insertStmt = connection
					.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			this.truncateStmt = connection
					.prepareStatement("TRUNCATE TABLE " + tableName);
			this.deleteStmt = connection
					.prepareStatement("DELETE FROM " + tableName + " WHERE id = ?");
			this.updateStmt = connection
					.prepareStatement(updateQuery);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}
	
	public int delete(Identifiable i){
		int count = 0;
		try {
			this.deleteStmt.setLong(1, i.getId());
			count = this.deleteStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}
	
	public int truncate(){
		int count = 0;
		try {
			count = this.truncateStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}

	protected long getLastInsertId(){
		long id = -1;
		try (ResultSet generatedKeys = this.insertStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }
            else {
                throw new SQLException("Creating " + this.tableName + " failed, no ID obtained.");
            }
        }
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return id;
	}
}
