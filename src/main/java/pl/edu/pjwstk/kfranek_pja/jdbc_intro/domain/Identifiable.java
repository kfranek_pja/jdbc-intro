package pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain;

//Intefrejs dla obiektow przechowywanych w bazie pod postacia wpisow z unikalnym ID

public interface Identifiable {

	public long getId();
	public void setId(long id);
	
}
