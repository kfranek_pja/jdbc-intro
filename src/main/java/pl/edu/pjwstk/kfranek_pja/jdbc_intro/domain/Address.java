package pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain;

//Klasa adresu

public class Address implements Identifiable {
	
	private long id = -1;
	private String street;
	private String buildingNumber;
	private String flatNumber;
	private String postalCode;
	private String city;
	private String country;
	
	public Address() {
		super();
	}
	
	public long getId(){
		return this.id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public void setStreet(String street){
		this.street = street;
	}
	
	public String getStreet(){
		return this.street;
	}
	
	public void setBuildingNumber(String buildingNumber){
		this.buildingNumber = buildingNumber;
	}
	
	public String getBuildingNumber(){
		return this.buildingNumber;
	}
	
	public void setFlatNumber(String flatNumber){
		this.flatNumber = flatNumber;
	}
	
	public String getFlatNumber(){
		return this.flatNumber;
	}
	
	public void setPostalCode(String postalCode){
		this.postalCode = postalCode;
	}
	
	public String getPostalCode(){
		return this.postalCode;
	}
	
	public void setCity(String city){
		this.city = city;
	}
	
	public String getCity(){
		return this.city;
	}
	
	public void setCountry(String country){
		this.country = country;
	}
	
	public String getCountry(){
		return this.country;
	}
}
