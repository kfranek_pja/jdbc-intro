package pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain;

import java.util.List;

//Klasa zamowienia

public class Order implements Identifiable  {
	
	private long id = -1;
	private ClientDetails client;
	private List<OrderItem> items;
	private Address deliveryAddress;
	
	public Order() {
	}
	
	public long getId(){
		return this.id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public ClientDetails getClient() {
		return this.client;
	}
	public void setClient(ClientDetails client) {
		this.client = client;
	}
	
	public Address getDeliveryAddress() {
		return this.deliveryAddress;
	}
	
	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	
	public List<OrderItem> getItems() {
		return this.items;
	}
	
	public void setItems(List<OrderItem> items) {
		this.items = items;
	}
	
}