package pl.edu.pjwstk.kfranek_pja.jdbc_intro.domain;

//Klasa pozycji na zamowieniu

public class OrderItem implements Identifiable  {
	
	private long id = -1;
	private String name;
	private String description;
	private double price;
	
	public OrderItem() {
	}
	
	public long getId(){
		return this.id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
}