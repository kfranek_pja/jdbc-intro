package pl.edu.pjwstk.kfranek_pja.jdbc_intro.query;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//Podstawowa klasa zapytania nad zbiorem wpisow posiadajacych unikalne ID

public abstract class IdentifiableQuery {

	protected Connection connection;
	protected Statement statement;


	private String url = "jdbc:hsqldb:hsql://localhost/testdb";
	private PreparedStatement getAllStmt;
	private PreparedStatement getByIdStmt;

	public IdentifiableQuery(String tableName, String createQuery) {
		try {
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();

			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tableExists = false;
			while (rs.next()) {
				if (tableName.equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}

			if (!tableExists)
				statement.executeUpdate(createQuery);

			getAllStmt = connection
					.prepareStatement("SELECT * FROM " + tableName);
			getByIdStmt = connection
					.prepareStatement("SELECT * FROM " + tableName + " WHERE id = ?");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	protected ResultSet getAllRes() {
		ResultSet rs = null;
		try {
			rs = getAllStmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rs;
	}
	
	public ResultSet getByIdRes(long id){

		try {
			getByIdStmt.setLong(1, id);
			ResultSet rs = getByIdStmt.executeQuery();
			if(rs.next()){
				return rs;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

}
